import emcee
import matplotlib.pyplot as plt
import numpy as np
import os


def plot_chain_diagnostics(samples, labels, nwalkers, print_autocorrelation_time=True, fig=None, axs=None):
    """
    Makes diagnostic plots of MCMC samples, including 3 plots per parameter, with ensemble dispersion,
    autocorrelation and mean and standard deviation across walkers.

    samples: np.ndarray
        array with shape (# of parameters, # of samples), where # of samples is # of steps times # of walkers.
    labels: array-like
        list with labels of parameters to be plotted.
    nwalkers: int
        number of walkers
    print_autocorrelation_time: bool (default: True)
        whether to print autocorrelation times for each parameter or not
    fig:  matplotlib.figure.Figure, optional
        matplotlib figure object to use when making the plots
    axs: np.ndarray of matplotlib.axes._subplots.AxesSubplot, optional
        matplotib axis to use when making plots. Should contain Nparameters lists, each with 3 axis objects.

    Returns:
        fig, axs
        fig: matplotlib.figure.Figure
            figure object holding plots
        axs: np.ndarray of matplotlib.axes._subplots.AxesSubplot, optional
            array with Nparameters list, each with 3 axis objects holding diagnostic plots.
    """
    try:
        Nparameters, Nsamples = samples.shape
    except ValueError:
        raise ValueError("samples should be 2-dimensional array of shape (Nparameters, Nsamples).")
    except AttributeError:
        raise AttributeError("samples should be 2-dimensional numpy array.")
    try:
        assert len(labels) == Nparameters
    except AssertionError:
        raise AssertionError("labels lenght should be the same as samples along first dimension.")

    if fig is None:
        fig = plt.figure(figsize=(3*Nparameters - 1, 10))
    if axs is None:
        axs = np.array([[fig.add_subplot(Nparameters, 3, i*3 + j) for j in range(1, 4)] for i in range(Nparameters)])

    try:
        assert axs.shape == (Nparameters, 3)
    except AssertionError:
        raise AssertionError("axs should be np.ndarray with shape (Nparameters, 3) of matplotlib axis.")

    for i, par in enumerate(samples):
        if axs[i][0].is_last_row():
            axs[i][0].set_xlabel("Passo (Ensemble)", fontsize=10)
            axs[i][1].set_xlabel("Passo (Ensemble)", fontsize=10)
            axs[i][2].set_xlabel("Passo (Walker)", fontsize=10)
        axs[i][0].set_ylabel(labels[i])
        if axs[i][0].is_first_row():
            axs[i][0].set_title("Dispersão", fontsize=12)
            axs[i][1].set_title("Autocorrelação", fontsize=12)
            axs[i][2].set_title("Média e desvio padrão", fontsize=12)
        idx = np.arange(len(par))
        mask = np.random.random(len(idx)) < min(1., 10000./len(idx))
        axs[i][0].scatter(idx[mask], par[idx][mask], marker='o', c='k', s=10.0, alpha=0.1, linewidth=0)
        ac = emcee.autocorr.function(par)
        if print_autocorrelation_time:
            try:
                print("Emcee integrated autocorrelation time for par ", labels[i], " :", emcee.autocorr.integrated_time(par))
            except:
                print("Chain to short to estimate autocorrelation time for par ", labels[i])

        idx = np.arange(len(ac),step=1)
        axs[i][1].scatter(idx, ac[idx], marker='o', c='k', s=10.0, alpha=0.1, linewidth=0)
        axs[i][1].axhline(alpha=1., lw=1., color='red')

        chains = np.array([par[t::nwalkers] for t in np.arange(nwalkers)])
        means = np.mean(chains, axis=0)
        stdmeans = np.std(chains, axis=0) / np.sqrt(nwalkers)
        idx = np.arange(len(means))
        axs[i][2].errorbar(x=idx, y=means, yerr=stdmeans, errorevery=100, ecolor='red',
                           lw=0.5, elinewidth=2., color='k')

    return fig, axs


def plot_chain_corner(samples, labels, burnin=0, weights=None, true_values=None, parameter_range=None):
    """
    Wraps corner to make corner plots, with true values marked.

    samples: np.ndarray
        array with shape (# of parameters, # of samples), where # of samples is # of steps times # of walkers.
    labels: array-like
        list with labels of parameters to be plotted.
    burnin:
        number of steps to discard
    weights: np.ndarray, optional
        array with shape (# of parameters) with weights to be given to samples
    true_values: list of floats, optional
        list holding true values of each parameter
    parameter_range: list of 2-tuples, optional
        list containing one 2-tuple for each parameter, holding the minimum and maximum values of the
        corresponding axis.

    Returns:
        fig, axs
        fig: matplotlib.figure.Figure
            figure object holding plots
        axs: np.ndarray of matplotlib.axes._subplots.AxesSubplot, optional
            array with Nparameters list, each with 3 axis objects holding diagnostic plots.
    """
    try:
        import corner
    except ImportError:
        raise ImportError("This functionality requires the package corner. "
                          "Please install it (pip install corner should do it)")
    try:
        Nparameters, Nsamples = samples.shape
    except ValueError:
        raise ValueError("samples should be 2-dimensional array of shape (Nparameters, Nsamples).")
    except AttributeError:
        raise AttributeError("samples should be 2-dimensional numpy array.")
    try:
        assert len(labels) == Nparameters
    except AssertionError:
        raise AssertionError("labels lenght should be the same as samples along first dimension.")

    samples_burned = np.c_[[par[burnin:] for par in samples]]
    try:
        weights_burned = weights[burnin:]
    except TypeError:
        weights_burned = None

    if parameter_range is not None:
        fig = corner.corner(samples_burned.T, labels=labels, weights=weights_burned,
                            quantiles=[0.16, 0.5, 0.84],
                            levels=(1-np.exp(-0.5), 1-np.exp(-2), 1-np.exp(-9./2)),
                            show_titles=True, title_kwargs={"fontsize": 12},
                            smooth1d=None, plot_contours=True,
                            no_fill_contours=False, plot_density=True,
                            range=parameter_range,
                           )
    else:
        fig = corner.corner(samples_burned.T, labels=labels, weights=weights_burned,
                            quantiles=[0.16, 0.5, 0.84],
                            levels=(1-np.exp(-0.5), 1-np.exp(-2), 1-np.exp(-9./2)),
                            show_titles=True, title_kwargs={"fontsize": 12},
                            smooth1d=None, plot_contours=True,
                            no_fill_contours=False, plot_density=True,
                           )
    # Extract the axes
    axes = np.array(fig.axes).reshape((Nparameters, Nparameters))

    if true_values is not None:
        # Loop over the diagonal
        for i in range(Nparameters):
            ax = axes[i, i]
            ax.axvline(true_values[i], color="r", ls="dashed")

        # Loop over the histograms
        for yi in range(Nparameters):
            for xi in range(yi):
                ax = axes[yi, xi]
                ax.axvline(true_values[xi], color="r", ls="dashed")
                ax.axhline(true_values[yi], color="r", ls="dashed")
                ax.plot(true_values[xi], true_values[yi], "sr")

    return fig, axes


